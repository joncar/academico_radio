import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

declare var MusicControls;
declare var Media;
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {  
  private media = undefined;
  constructor(public navCtrl: NavController) {
  		if(typeof(Media)!=='undefined'){
  			

	  		MusicControls.create({
			    track       : '100.7 FM UNP',		// optional, default : ''
				artist      : 'Muse',						// optional, default : ''
			    cover       : 'assets/imgs/logounp.png',		// optional, default : nothing
				isPlaying   : true,							// optional, default : true
				dismissable : true,
				hasPrev   : false,		// show previous button, optional, default: true
				hasNext   : false,		// show next button, optional, default: true
				hasClose  : true,		// show close button, optional, default: false
				album       : '100.7 FM UNP',     // optional, default: ''
				playIcon: 'media_play',
				pauseIcon: 'media_pause',
				prevIcon: 'media_prev',
				nextIcon: 'media_next',
				closeIcon: 'media_close',
				notificationIcon: 'notification'
			});			

			// Register callback
			MusicControls.subscribe(this.escucharTabBar);

			// Start listening for events
			// The plugin will run the events function each time an event is fired
			MusicControls.listen();
  		};
  }

  public escucharTabBar(action){
	  	const message = JSON.parse(action).message;
	  	let elemento = <HTMLVideoElement>document.getElementById('elemento');
		switch(message) {					
			case 'music-controls-pause':				
				elemento.pause();
			break;
			case 'music-controls-play':
				elemento.play();
			break;
			case 'music-controls-destroy':
				this.media.pause();
			break;
		}
  }

  public start($event){
  	if(typeof(Media)!=='undefined'){
  		this.media.start();
  	}
  }

  public stop($event){
  	if(typeof(Media)!=='undefined'){
  		this.media.pause();
  	}
  }



}
